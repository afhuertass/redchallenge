package com.climbingcolor;

public interface AndroidInterface {

	public void postScore(int score);
	public void loadAd();
	public void showAd();
	public void showMoreGames();
	public void startLoginLibgdx(int score);
}
