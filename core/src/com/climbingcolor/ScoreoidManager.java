package com.climbingcolor;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Net.HttpMethods;
import com.badlogic.gdx.Net.HttpRequest;
import com.badlogic.gdx.Net.HttpResponse;
import com.badlogic.gdx.Net.HttpResponseListener;
import com.badlogic.gdx.net.HttpParametersUtils;
import com.badlogic.gdx.scenes.scene2d.ui.List;
import com.badlogic.gdx.utils.Json;
import com.badlogic.gdx.utils.JsonReader;
import com.badlogic.gdx.utils.JsonValue;


public class ScoreoidManager {
// esta clase manejara la conexion con Scoreoid. por el momento solo nos interesa obtener los scores 
	/*
	 * para no sufrir con sistemas de login. se asignara los usuarios simplemente por numero 1...2...2 as{i hasta
	 * el millon que vamos a conseguir :D 
	 */
	public static final String API_KEY = "dde622f32cb1e95bafd5f44a1a5b3f65a260a10a";
	public static final String GAME_ID = "06729ea2fa";
	public static final String URL = "http://api.scoreoid.com/v1/"; // url para acceder al API
	Json json;
	Bundle bundle;
	Map<String, String>	 parametros = new HashMap<String,String>();
	List<Player> players;
	ScoreoidManager(Bundle b){
		json = new Json();
		bundle = b;
		parametros.put("api_key", API_KEY);
		parametros.put("game_id", GAME_ID);
		parametros.put("response", "json");
		parametros.put("default", "0");
		
	}
	public int getLastPlayer(){
				
		/* parametros.put("api_key", API_KEY);
		parametros.put("game_id", GAME_ID);
		parametros.put("response", "json");
		parametros.put("default", "0");
		*/
		parametros.put("key", "last");
		HttpRequest request = new HttpRequest(HttpMethods.POST);
		
		request.setUrl(URL+"getGameData");
		//Gdx.app.log("Climb", json.toJson(parametros));
		//request.setHeader("ContentType", "application/x-www-form-urlencoded");
		//request.ContentType = "application/x-www-form-urlencoded";
		request.setContent(HttpParametersUtils.convertHttpParameters(parametros));
		Gdx.app.log("Climb", "shit");
	
		Gdx.net.sendHttpRequest(request, new HttpResponseListener(){

			@Override
			public void handleHttpResponse(HttpResponse httpResponse) {
				// TODO Auto-generated method stub
				String res = httpResponse.getResultAsString();
				//Gdx.app.log("Climb", httpResponse.getStatus());
				
				GameDataStore data = json.fromJson(GameDataStore.class, res);
				Gdx.app.log("Climb", data.last);
				registerPlayer(Integer.parseInt(data.last) );
			}

			@Override
			public void failed(Throwable t) {
				// TODO Auto-generated method stub
				
				Gdx.app.log("Climb", t.getMessage());
			}

			@Override
			public void cancelled() {
				// TODO Auto-generated method stub
				
			}
			
		});
		return 0;
	}
	public boolean registerPlayer(final int last){
		/*
		 *  registramos el que es last+1
		 * primero obtenemos si hay un jugador registrado ya en las preferencias
		 * si no 
		 */
		//Gdx.app.log("Climb", Integer.toString(bundle.game.scoreManager.getUserId()));
		if(bundle.game.scoreManager.getUserId() != 0) return true;
		parametros.put("api_key", API_KEY);
		parametros.put("game_id", GAME_ID);
		parametros.put("response", "json");
		parametros.put("username", Integer.toString(last+1));
		HttpRequest request = new HttpRequest( HttpMethods.POST);
		request.setContent(HttpParametersUtils.convertHttpParameters(parametros));
		request.setUrl(URL+"createPlayer");
		Gdx.net.sendHttpRequest(request, new HttpResponseListener(){

			@Override
			public void handleHttpResponse(HttpResponse httpResponse) {
				// TODO Auto-generated method stub
				Gdx.app.log("Created Player", httpResponse.getResultAsString());
				bundle.game.scoreManager.savePlayerId(last+1);
			}

			@Override
			public void failed(Throwable t) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void cancelled() {
				// TODO Auto-generated method stub
				
			}
			
		});
		return false;
	}
	public void publishBestScore(){
		parametros.put("username", Integer.toString(bundle.game.scoreManager.getUserId()));
		parametros.put("score", Integer.toString(bundle.game.scoreManager.getHighScore() ) );
		parametros.put("platform", "android");
		
		HttpRequest request = new HttpRequest(HttpMethods.POST);
		request.setContent(HttpParametersUtils.convertHttpParameters(parametros));
		request.setUrl(URL+"createScore");
		Gdx.net.sendHttpRequest(request, new HttpResponseListener(){

			@Override
			public void handleHttpResponse(HttpResponse httpResponse) {
				// TODO Auto-generated method stub
				Gdx.app.log("Climb", httpResponse.getResultAsString());
			}

			@Override
			public void failed(Throwable t) {
				// TODO Auto-generated method stub
				// FAIL
			}

			@Override
			public void cancelled() {
				// TODO Auto-generated method stub
				
			}
			
		});
	}
	public void getBestScore(){
		parametros.put("api_key", API_KEY);
		parametros.put("game_id", GAME_ID);
		parametros.put("order_by", "score");
		parametros.put("order", "desc");
		parametros.put("limit", "1,1");
		
		HttpRequest request = new HttpRequest(HttpMethods.POST);
		request.setContent(HttpParametersUtils.convertHttpParameters(parametros));
		request.setUrl(URL+"getBestScores");
		Gdx.net.sendHttpRequest(request, new HttpResponseListener(){

			@Override
			public void handleHttpResponse(HttpResponse httpResponse) {
				// TODO Auto-generated method stub
				//Gdx.app.log("Climb", httpResponse.getResultAsString());
				String result = httpResponse.getResultAsString();
				String result2 = result.substring(1, result.length()-1);
				result2 = result2.toLowerCase();
				String result3[] = result2.split(",");
				String score[] = result3[5].split(":");
				bundle.setBestScore(score[2].substring(1, score[2].length() -1));
				//json.setElementType(Players.class, "Player", Player.class);
				//int bestScore = Integer.parseInt(score[2]);
				//Players player = json.fromJson(Players.class, result);
				//Gdx.app.log("Climb", result2);
				//Players p = json.fromJson(Players.class, result2);
				//Player p = json.fromJson(Player.class, result2);
				
				
				//Gdx.app.log("Climb", result2);
				//json.setElementType(Players.class, "Player", Player.class);
				//Players players = new Players();
				//players = json.fromJson(Players.class, httpResponse.getResultAsString());
				
			}

			@Override
			public void failed(Throwable t) {
				// TODO Auto-generated method stub
				bundle.setBestScore("x");
			}

			@Override
			public void cancelled() {
				// TODO Auto-generated method stub
				
			}
			
		});
	}
	
}
