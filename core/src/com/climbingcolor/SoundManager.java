package com.climbingcolor;

import java.util.ArrayList;
import java.util.Random;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;

public class SoundManager {
/*
 * Ahora el sound manager debe encargarse de seleccionar songs de la lista,	
 */
ArrayList<String> songs;
String currentSong;
String oldSong;
int repetitions = 0;
public float volume = 0.8f;
Random rnd = new Random();
MainClimb game;

	SoundManager(MainClimb g){
	this.game = g;
	songs = new ArrayList<String>();
	//songs.add("run.ogg"); 
	songs.add("builder.ogg"); songs.add("monkeys.ogg"); songs.add("run.ogg");
	
	currentSong = "builder.ogg";
	}
	public void playMainLoop(){
		
		if(game.loader.manager.isLoaded("data/sounds/"+this.currentSong)){
			if(game.loader.manager.get("data/sounds/"+this.currentSong , Music.class).isPlaying() ) return ;
			game.loader.manager.get("data/sounds/" + this.currentSong, Music.class).setLooping(true);
			game.loader.manager.get("data/sounds/" + this.currentSong, Music.class ).setVolume(volume);
			game.loader.manager.get("data/sounds/" + this.currentSong, Music.class ).play();
			
		}
	}
	public void stopMainLoop(){
		
		if(game.loader.manager.isLoaded("data/sounds/"+this.currentSong)) {
			game.loader.manager.get("data/sounds/"+this.currentSong, Music.class).pause();
		}
		this.changeSound();
	}
	public void playPop(){
		if(game.loader.manager.isLoaded("data/sounds/popB.mp3")){
			game.loader.manager.get("data/sounds/popB.mp3" , Sound.class).play(0.4f);
		}
	}
	public void playCombo(){
		if(game.loader.manager.isLoaded("data/sounds/ting.mp3" , Sound.class)){
			game.loader.manager.get("data/sounds/ting.mp3" , Sound.class).play();
		}
	}
	public void playWrong(){
		if(game.loader.manager.isLoaded("data/sounds/wrong1.ogg")){
			game.loader.manager.get("data/sounds/wrong1.ogg" , Sound.class).play(0.2f);
		}
	}
	public void changeSound(){
		if(this.repetitions >= 1){
			// hay que seleccionar nueva canción.
			int n = rnd.nextInt(songs.size());
			this.disposeCurrentLoadNew(songs.get(n));
			currentSong = songs.get(n);
			Gdx.app.log("Climb", "Playing " + currentSong );
			
			this.repetitions = 0;
			return ; 
		}
		this.repetitions += 1;
	}
	public void disposeCurrentLoadNew(String nuevo){
		// hacer dispose del sonido actual y comenzar a cargar el nuevo.
		// la razon es que las instancias de Music son muy pesadas
		game.loader.manager.get("data/sounds/"+currentSong , Music.class).stop();
		game.loader.manager.get("data/sounds/"+currentSong , Music.class).dispose();
		game.loader.manager.load("data/sounds/"+nuevo , Music.class);
	}
	public boolean isLoadingSound(){
		
		return game.loader.manager.isLoaded("data/sounds/" + currentSong );
		
	}
}
