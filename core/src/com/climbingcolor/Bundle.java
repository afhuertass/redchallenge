package com.climbingcolor;

import java.math.BigDecimal;
import java.util.Random;

import aurelienribon.tweenengine.Tween;
import aurelienribon.tweenengine.TweenEquations;
import aurelienribon.tweenengine.TweenManager;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;


public class Bundle {
	int REPETICIONES_REVMOB = 3;
	int score = 0;
	float runningTime = 0;
	float time = 30;
	float deltX=100;
	float deltY = 120;
	float currentY = 200;
	float anchoInicialTime = 0;
	float timeCombos = 1.3f;
	float speed = 2f;
	float comboTimeCounter = 0;
	int comboCounter = 0;
	float factorTime = 1;
	int counterSlalom = 0;
	int counterSlaloms = 0;
	MainClimb game;
	Image gameOver; // fondo game over;
	Label scoreLabel;
	Label timeLabel;
	Label parcialScore;
	Label bestScore;
	Label bestWorldScore;
	Image facebook;
	Image timeBar;
	Image combo3;
	Image combo5;
	Image combo7;
	Image comboSlalom;
	Image comboDoubleSlalom;
	Image timeBarRed;
	ImageButton reset;
	ImageButton others;
	Random random;
	TweenManager tweenManager;
	TweenManager tweenManagerCombos;
	Group balls = new Group();
	Group gameOverE = new Group();
	Group timeBars = new Group();
	Ball lastb;
	Ball selected = null;
	// textureas de las bolitas
	TextureRegionDrawable red;
	TextureRegionDrawable redOn;
	TextureRegionDrawable blue;
	TextureRegionDrawable yellow;
	TextureRegionDrawable orange;
	TextureRegionDrawable conexion;
	TextureRegionDrawable timeBarTexture;
	/*
	 * Scoreoid Manager
	 */
	ScoreoidManager scoreoidManager;
	SoundManager soundManager;
	ComboShower comboShower;
	boolean isOver = false;
	boolean isCountingCombo = false;
	int playedGames = 0;
	ParticleActor stars;
	ParticleActor end;
	Bundle(MainClimb g){
		this.game = g;
		//this.loadGameOver();
		random = new Random();
		Tween.registerAccessor(Image.class, new ImageAccessor());
		Tween.registerAccessor(ParticleActor.class, new ParticleAccessor());
		Tween.registerAccessor(SoundManager.class, new SoungManagerAccessor()); // para tweenear el volumen
		this.tweenManager = new TweenManager();
		this.tweenManagerCombos = new TweenManager();
		scoreoidManager = new ScoreoidManager(this);
		this.soundManager = new SoundManager(this.game);
	}
	public void loadTextures(){ // 
		this.red = new TextureRegionDrawable(game.loader.manager.get("data/atlas/main.pack" ,TextureAtlas.class).findRegion("red"));
		this.blue = new TextureRegionDrawable(game.loader.manager.get("data/atlas/main.pack" ,TextureAtlas.class).findRegion("blue"));
		this.orange = new TextureRegionDrawable(game.loader.manager.get("data/atlas/main.pack" ,TextureAtlas.class).findRegion("naran"));
		this.yellow = new TextureRegionDrawable(game.loader.manager.get("data/atlas/main.pack" ,TextureAtlas.class).findRegion("yellow"));
		this.redOn = new TextureRegionDrawable(game.loader.manager.get("data/atlas/main.pack" ,TextureAtlas.class).findRegion("redOn"));
		this.conexion = new TextureRegionDrawable(game.loader.manager.get("data/atlas/main.pack" ,TextureAtlas.class).findRegion("conex"));
		this.timeBarTexture = new TextureRegionDrawable(game.loader.manager.get("data/atlas/main.pack" ,TextureAtlas.class).findRegion("timeBar") );
		this.timeBar = new Image(timeBarTexture);
		// cargar imagenes
		timeBar.setPosition(0, 1280-timeBar.getHeight());
		
		this.combo3 = new Image(new TextureRegionDrawable(game.loader.manager.get("data/atlas/main.pack" ,TextureAtlas.class).findRegion("combo3") ) );
		this.combo5 = new Image(new TextureRegionDrawable(game.loader.manager.get("data/atlas/main.pack" ,TextureAtlas.class).findRegion("combo5") ) );
		this.combo7 = new Image(new TextureRegionDrawable(game.loader.manager.get("data/atlas/main.pack" ,TextureAtlas.class).findRegion("combo7") ) );
		this.comboSlalom = new Image(new TextureRegionDrawable(game.loader.manager.get("data/atlas/main.pack" ,TextureAtlas.class).findRegion("comboSlalom") ) );
		this.comboDoubleSlalom = new Image(new TextureRegionDrawable(game.loader.manager.get("data/atlas/main.pack" ,TextureAtlas.class).findRegion("comboDoubleSlalom") ) );
		this.timeBarRed =  new Image(new TextureRegionDrawable(game.loader.manager.get("data/atlas/main.pack" ,TextureAtlas.class).findRegion("timeBarRed") ) );
		this.anchoInicialTime = timeBar.getWidth();
		timeBarRed.setPosition(720,1280- timeBarRed.getHeight());
		// done bitch
		this.combo3.setPosition(100, 1280); this.combo3.setTouchable(Touchable.disabled);
		this.combo5.setPosition(100, 1280); this.combo5.setTouchable(Touchable.disabled);
		this.combo7.setPosition(100, 1280); this.combo7.setTouchable(Touchable.disabled);
		this.comboSlalom.setPosition(100, 1280); this.comboSlalom.setTouchable(Touchable.disabled);
		this.comboDoubleSlalom.setPosition(100, 1280); this.comboDoubleSlalom.setTouchable(Touchable.disabled);
		this.timeBars.addActor(timeBar);
		this.timeBars.addActor(timeBarRed);
		this.stars = new ParticleActor("data/effects/effect2" , game.loader.manager.get("data/atlas/main.pack" , TextureAtlas.class));
		this.end = new ParticleActor("data/effects/effect3" , game.loader.manager.get("data/atlas/main.pack" , TextureAtlas.class));
		this.loadGameOver();
		this.comboShower = new ComboShower(this);
		
	}
	public TextureRegionDrawable getTexture(int color){
		switch(color){
		case 0:
			return red;
			
		case 1:
			return blue;
		case 2: 
			return yellow;
		case 3:
			return orange;
		}
		return null;
	}
	public void update(float sec){
		if(!isOver){
		runningTime += sec;
		time -= sec*this.factorTime;
		time = this.round(time, 2);
		}
		if(this.runningTime >=40){
			this.speed = 1.5f;
			this.factorTime = 2f;
			//this.runningTime = 40;
		}
		
		if(time <= 0 && !isOver){
			isOver = true;
			time = 0;
			showGameOver();
			 if(this.game.scoreManager.saveScore(score)){
				 // mejoro el score actual
				 scoreoidManager.publishBestScore();
			 }
			this.bestScore.setText(Integer.toString(game.scoreManager.getHighScore()));
		}
		this.addLine();
		this.timeBars.toFront();
		this.tweenManager.update(sec);
		this.tweenManagerCombos.update(sec);
		this.updateGameOvers(); // mantener actualizadas las posiciones de los game over.
		//this.timeLabel.setText(Float.toString(time));
		this.scoreLabel.setText(Integer.toString(score));
		this.parcialScore.setText(Integer.toString(score));
		this.updateTimeScale();
		this.clean();
		this.checkCombos(); // check for combos x3 x5 x7 slalom y double slalom
		//Gdx.app.log("Climb", Integer.toString(this.comboCounter));
		if(!this.isOver)this.soundManager.playMainLoop();
	}
	public void checkCombos(){
		// check for slalom.
		
		if(this.counterSlalom == 2 ){
			// display slalom 
			
			this.counterSlalom = 0;
			this.counterSlaloms += 1;
			if(counterSlaloms != 2){
				this.comboShower.showCombo(0);
				this.soundManager.playCombo();
				// agregar tiempo , cuanto?
				this.time += 0.2;
			}
		}
		if(this.counterSlaloms == 2){
			this.comboShower.showCombo(1);
			this.soundManager.playCombo();
			this.counterSlaloms = 0;
			this.counterSlalom = 0;
			this.time += 0.2;
			return ;
		}
		
	}
	public void updateTimeScale(){
	 timeBar.setWidth(time/30*this.anchoInicialTime);
	 timeBar.toFront();
	 timeBarRed.setWidth(720-timeBar.getWidth()+15);
	 timeBarRed.setX(timeBar.getWidth()-10);
	 
	}
	public void clean(){
		for(Actor ac : balls.getChildren()){
			if(ac.getY() < -100) {
				
				ac.remove();
			}
		}
	}
	public void addLine(){
		/*
		 * agregar una linea de bolitas maximo 3 en 
		 * 
		 */
		if(this.calculateDelta() < 100){
			//Gdx.app.log("Climb", "delta peque ");
			return ;
		}
		int max = 2;//random.nextInt(3);
		float x = 230;
		
	
		for(int i = 0; i <=max ; i++){
			
			
			//x = min + (maxr-min)*random.nextFloat();
			
			Ball b = new Ball(this,x,this.currentY); 
			b.colum = i; // columnas 0 1 2 
			b.getDelta();
			this.lastb = b;
			this.balls.addActor(b);
			this.balls.addActor(b.ball);
			
			x += 190;
		}
		//Gdx.app.log("Climb", "nuevas");
		currentY += this.deltY;
		//Gdx.app.log("Climb",Float.toString(currentY));
	}
	public float  calculateDelta(){
		if( this.lastb == null) return 100;
		//Gdx.app.log("Climbing", Float.toString(this.lastb.ball.getY()));
		currentY = lastb.ball.getY() + this.deltY;
		return Math.abs(1190 - this.lastb.ball.getY());
		
		
	}
	public void calculateAllDeltas(){
		for(Actor ball : this.balls.getChildren()){
			if(ball instanceof Ball){
				if(ball.equals(this.selected)) continue;
				((Ball) ball).getDelta();
			}
		}
	}
	public void startTween(){
		Tween.to(selected.ball, ImageAccessor.POSITION_Y, this.speed).target(200).ease(TweenEquations.easeOutQuart).start(tweenManager);
		//this.TweenScale(selected);
	}
	public void loadGameOver(){
		this.gameOver = new Image( game.loader.manager.get("data/atlas/main.pack" , TextureAtlas.class).findRegion("gameOver"));
		LabelStyle estilo = new LabelStyle();
		estilo.font = game.loader.manager.get("data/fonts/font.fnt");
		this.scoreLabel = new Label("", estilo);
		//this.timeLabel = new Label("", estilo); // si queda as{i remover 
		//this.timeLabel.setVisible(false);
		this.bestScore = new Label(Integer.toString(game.scoreManager.getHighScore()), estilo);
		this.parcialScore = new Label("0", estilo);
		this.bestWorldScore = new Label("", estilo);
		this.facebook = new Image(game.loader.manager.get("data/atlas/main.pack", TextureAtlas.class).findRegion("fb"));
		this.reset = new ImageButton( new TextureRegionDrawable(game.loader.manager.get("data/atlas/main.pack", TextureAtlas.class).findRegion("reset") ) , new TextureRegionDrawable(game.loader.manager.get("data/atlas/main.pack", TextureAtlas.class).findRegion("resetOn") ));
		this.others = new ImageButton( new TextureRegionDrawable(game.loader.manager.get("data/atlas/main.pack", TextureAtlas.class).findRegion("next") ) , new TextureRegionDrawable(game.loader.manager.get("data/atlas/main.pack", TextureAtlas.class).findRegion("nextOn") ));
		this.gameOver.setPosition(50, 1280);
		this.parcialScore.setPosition(95, 1034);
		this.bestScore.setPosition(95, 930);
		this.bestWorldScore.setPosition(95, 850);
		reset.setPosition(gameOver.getX() + gameOver.getWidth()/2-50, gameOver.getY() + others.getHeight()+30);
		scoreLabel.setPosition(gameOver.getX()+gameOver.getWidth()-150, gameOver.getY() + 25);
		facebook.setPosition(gameOver.getX() + 50,  gameOver.getY() + others.getHeight()+20);
		others.setPosition(gameOver.getX() + gameOver.getWidth()-150, gameOver.getY() + others.getHeight()+30);
		this.gameOverE.addActor(gameOver);
		this.gameOverE.addActor(others);
		this.gameOverE.addActor(facebook);
		this.gameOverE.addActor(reset);
		this.gameOverE.addActor(scoreLabel);
		this.gameOverE.addActor(combo3);
		this.gameOverE.addActor(combo5);
		this.gameOverE.addActor(combo7);
		this.gameOverE.addActor(comboSlalom);
		this.gameOverE.addActor(comboDoubleSlalom);
		//this.timeLabel.setPosition(80, 1185); // 1120
		//game.main.elements.addActor(timeLabel);
		game.main.elements.addActor(parcialScore);
		game.main.elements.addActor(bestScore);
		game.main.elements.addActor(bestWorldScore);
		game.main.elements.addActor(stars);
		game.main.elements.addActor(end);
		this.addListeners();
		this.scoreoidManager.getBestScore();
	}
	public void showGameOver(){
		// hace un tween. para mostrar el gameOver y apagar la musica entre otros
		// 
		Tween.to(gameOver, ImageAccessor.POSITION_Y, 1.5f).target(600).ease(TweenEquations.easeOutBack).start(tweenManager);
		balls.addAction(Actions.fadeOut(0.5f));
		//this.tweenVolumeToMin();
		soundManager.stopMainLoop();
		// FOR ADS 
		if(playedGames == this.REPETICIONES_REVMOB){
			game.androidI.showAd();
			playedGames = 0;
		}
		scoreoidManager.getBestScore();
		playedGames += 1;
	}
	public void hideGameOver(){
		Tween.to(gameOver, ImageAccessor.POSITION_Y, 1.5f).target(1280).ease(TweenEquations.easeOutBack).start(tweenManager);
		balls.addAction(Actions.sequence( Actions.fadeIn(0.5f) ));
	}
	public void updateGameOvers(){
		// catualizar las posiciones de los game over elements en funcion de el game over.
		reset.setPosition(gameOver.getX() + gameOver.getWidth()/2, gameOver.getY() + others.getHeight()+30);
		scoreLabel.setPosition(gameOver.getX()+gameOver.getWidth()-180, gameOver.getY() + 55);
		facebook.setPosition(gameOver.getX() + 50,  gameOver.getY() + others.getHeight()+20);
		others.setPosition(gameOver.getX() + gameOver.getWidth()-150, gameOver.getY() + others.getHeight()+30);
	}
	public void addListeners(){
		// agregar los Listeners al game over
		/*
		 * cuando le de a reset. oculta el game over y muestra nuevamente las peloticas.
		 * el total de puntos lo pasa a cero y el tiempo otra vez a 30.
		 * 
		 * FACEBOOK POR VER
		 * 
		 * OTHERS POR VER-
		 */
		reset.addListener(new InputListener(){
			@Override
			public boolean touchDown(InputEvent event, float x, float y, int pointer, int button){
				hideGameOver();
				time = 30;
				score = 0;
				runningTime = 0;
				isOver = false;
				counterSlalom = 0;
				selected = null;
				factorTime = 1;
				speed = 2;
				comboCounter = 0;
			
				return true;
			}
			});
		this.others.addListener(new InputListener(){
			@Override
			public boolean touchDown(InputEvent event, float x, float y, int pointer, int button){
				game.androidI.showMoreGames();
				return true;
			}
		});
		this.facebook.addListener(new InputListener(){
			@Override
			public boolean touchDown(InputEvent event, float x, float y, int pointer, int button){
				game.androidI.startLoginLibgdx(score);
				return true;
			}
		});
	}
	 public float round(float d, int decimalPlace) {
	        BigDecimal bd = new BigDecimal(Float.toString(d));
	        bd = bd.setScale(decimalPlace, BigDecimal.ROUND_HALF_UP);
	        return bd.floatValue();
	    }
	 public void TweenScale(Ball b){
		 Tween.to(b.ball, ImageAccessor.SCALE_X, 1f).target(0.5f).ease(TweenEquations.easeOutElastic).start(this.tweenManager);
	 }
	 public void checkSlalom(Ball b){
		 // debe chechear si va en slalom // AQUI TENGO CERTAZA DE QUE SELECTED NO ES NULL
		
		 if(b.colum == 0 || b.colum == 2){
			 this.counterSlalom = 1; 
		 }
		 if(this.counterSlalom == 5){
			 this.counterSlalom = 2;
		 }
		 if(selected.colum == 0 &&  2 == b.colum){
			 this.counterSlalom += 1;
		 }else {
		 if(selected.colum == 2 && 0 == b.colum ){
			 this.counterSlalom += 1;
		 }else {
			 	this.counterSlalom = 0;
		 }
		 
		 }
		 
	 }
	 public void checkCombo(){
		 
		 if(this.comboCounter == 0){
			// this.comboCounter += 1;
			 this.comboTimeCounter = this.runningTime;
			 //return;
		 }
		 this.comboCounter +=1;
		 if(comboCounter == 3 ){
			 if(Math.abs(runningTime - comboTimeCounter) <= 1.5) {
				Gdx.app.log("Climb", "COMBO X3"); 
			 this.comboShower.showCombo(2);
			 soundManager.playCombo();
			 this.time += 0.2;
			 //this.score += 50;
			 }else {
				 this.comboTimeCounter = 0;
			 }
		 }
		 if(comboCounter == 5){
			 if((this.runningTime - this.comboTimeCounter) <= 1.5){
				 this.comboShower.showCombo(3); // 3 = show combo x5
				 soundManager.playCombo();
				 this.time += 0.2;
			 }else {
				 this.comboTimeCounter = 0;
			 }
		 }
		 if(comboCounter == 7){
			 
			 if((this.runningTime - this.comboTimeCounter) <=1.5 ){
				 this.comboShower.showCombo(4); // 3 = show combo 7
				 soundManager.playCombo();
				 this.time += 3;
			 }else {
				 this.comboTimeCounter = 0;
			 }
		 }
		 if(comboCounter > 7 ) comboCounter = 0;
		 
		 
	 }
	public void setBestScore(String best){
		this.bestWorldScore.setText(best);
	}
	public void showParticle(float x, float y){
		this.stars.start();
		this.stars.setPosition(x, y);
		this.stars.effect.setPosition(x, y);
	}
	public void showParticleEnd(float x, float y){
		this.end.start();
		this.end.setPosition(x, y);
		this.end.effect.setPosition(x, y);
	}
	public void tweenVolumeToMax(){
		Tween.to(soundManager, SoungManagerAccessor.VOLUME, 1f).target(1f).ease(TweenEquations.easeInCubic).start(tweenManager);
	}
	public void tweenVolumeToMin(){
		Tween.to(soundManager, SoungManagerAccessor.VOLUME, 1f).target(0f).ease(TweenEquations.easeInCubic).start(tweenManager);
	}
	public void chechForReds(){
		
	}
}
