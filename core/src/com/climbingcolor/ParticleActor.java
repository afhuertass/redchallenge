package com.climbingcolor;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.ParticleEffect;

import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.scenes.scene2d.Actor;

public class ParticleActor extends Actor {
	ParticleEffect effect;
	float x=0,y=0; // posicion del efecto, la idea es Tweenearlos
	ParticleActor(String name,TextureAtlas atlas){
		// cargar el nombre del efecto y usar el atlas
		effect = new ParticleEffect();
		effect.load(Gdx.files.internal(name),atlas);
		//effect.start();
		this.setPosition(200, 1130);
		effect.setPosition(200, 1130);
	}
	public void start(){
		effect.start();
	}
	@Override
	public void act(float delta){
		//effect.setX(this.getX()); //, 1130);
		effect.setPosition(this.getX(), this.getY());
		effect.update(delta);
		
		//Gdx.app.log("Cimb", "aaaa");
		if(effect.isComplete()){
			//effect.dispose();
			//effect.start();
			effect.setPosition(200,1100);
		}
	}
	@Override
	public void draw(Batch batch, float parentAlpha){
		effect.draw(batch);
	}
	public void dispose(){
		effect.dispose();
	}
	public void setInitial(){
		effect.setPosition(200, 1130);
		
	}
}
