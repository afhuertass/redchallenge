package com.climbingcolor;


import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public class MainClimb extends Game {
	SpriteBatch batch;
	Texture img;
	MainScreen main;
	AssetsLoader loader;
	Preferences mainPref;// = Gdx.app.getPreferences("prefClimbig");
	ScoreManager scoreManager;
	AndroidInterface androidI;
	public MainClimb(AndroidInterface inter){
		this.androidI = inter;
		//this.androidI.postScore(0);
	}
	@Override
	public void create () {
		loader = new AssetsLoader();
		main =  new MainScreen(this);
		mainPref = Gdx.app.getPreferences("prefClimb");
		scoreManager = new ScoreManager(mainPref);
		
		this.setScreen(main);
	}
	@Override
	public void dispose(){
		loader.manager.clear();
		loader.manager.dispose();
	}

}
