package com.climbingcolor;

import com.badlogic.gdx.scenes.scene2d.ui.Image;

import aurelienribon.tweenengine.TweenAccessor;

public class ImageAccessor  implements TweenAccessor<Image>{
public static final int POSITION_Y = 1;
public static final int SCALE_X = 2;
	@Override
	public int getValues(Image arg0, int  type, float[] arg2) {
		// TODO Auto-generated method stub
		switch(type){
		case POSITION_Y:
			arg2[0] = arg0.getY();
			return 1;
		case SCALE_X:
			arg2[0] = arg0.getScaleX();
			return 1;
		default: assert false; return -1;
			
		}
		
	}

	@Override
	public void setValues(Image arg0, int arg1, float[] arg2) {
		// TODO Auto-generated method stub
		switch(arg1){ // swithc tipo
		case POSITION_Y:
			arg0.setY(arg2[0]);
			break;
		case SCALE_X:
			arg0.setScale(arg2[0]);
			break;
		default: assert false; break;	
		}
	}
}
