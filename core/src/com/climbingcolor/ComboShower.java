package com.climbingcolor;

import aurelienribon.tweenengine.Timeline;
import aurelienribon.tweenengine.Tween;
import aurelienribon.tweenengine.TweenEquations;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.ParticleEffect;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.scenes.scene2d.ui.Image;

public class ComboShower {
// esta clase recibe el bundle y va a mostrar los efectos 
	/*
	 * la idea es que un metodo showCombos(int) reciba un identificador del combo que debe desplegar
	 * selecciona la imagen del bundle. y aplica los Tweens. 
	 * luego tomamos un efecto y le hacemos Tween tambien  a los efectos que voy a tener 
	 * guardados en combo shower
	 */
	Bundle bundle;
	ParticleActor effect1;
	float yPosition = 1100; 
	ComboShower(Bundle b){
		this.bundle = b;
		
		this.loadEffects();
	}
	public void loadEffects(){
		effect1 = new ParticleActor("data/effects/effect1" , bundle.game.loader.manager.get("data/atlas/main.pack" , TextureAtlas.class));
				
		bundle.game.main.stage.addActor(effect1); // agrega al stage
	}
	public void showCombo(int c){
		Image punt = null;
		this.checkInterpolations();
		switch(c){
		case 0: // combo slalom
			//Tween.to(bundle.comboSlalom, ImageAccessor.POSITION_Y, 1f).target(1100).ease(TweenEquations.easeOutElastic).repeatYoyo(2, 0).start(bundle.tweenManager);
			//Timeline.createSequence().push(Tween.to(bundle.comboSlalom, ImageAccessor.POSITION_Y, 1.5f).target(1100).ease(TweenEquations.easeOutElastic)).push(Tween.to(bundle.comboSlalom, ImageAccessor.POSITION_Y, 0.8f).target(1380).ease(TweenEquations.easeOutQuint)).start(bundle.tweenManager);
			punt = bundle.comboSlalom;
			break;
		case 1: 
			punt = bundle.comboDoubleSlalom;
			break;
		case 2:
			punt = bundle.combo3;
			break;
		case 3:
			punt = bundle.combo5;
			break;
		case 4:
			punt = bundle.combo7;
			break;
		default : return;	
		}
		Timeline.createSequence().push(Tween.to(punt, ImageAccessor.POSITION_Y, 0.8f).target(yPosition).ease(TweenEquations.easeOutElastic)).push(Tween.to(punt, ImageAccessor.POSITION_Y, 0.8f).target(1280).ease(TweenEquations.easeOutQuint)).start(bundle.tweenManagerCombos);
		this.TweenParticle();
	}
	public void checkInterpolations(){
		yPosition = 1100 - bundle.tweenManagerCombos.size()*bundle.combo3.getHeight()*0.5f;
		
	}
	public void TweenParticle(){
		effect1.start();
		effect1.setPosition(200, 1130);
		Tween.to(effect1, ParticleAccessor.POSITION_X, 1.5f).target(600).ease(TweenEquations.easeOutExpo).start(bundle.tweenManager);
	}
}
