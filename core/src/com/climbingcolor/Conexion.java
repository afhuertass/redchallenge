package com.climbingcolor;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.Image;

public class Conexion extends Actor  {

	Ball B1;
	Ball B2;
	Bundle bundle;
	Image conex;
	float deltax;
	float deltay;
	float dis;
	float scale;
	float angle,x,y;
	Conexion(Bundle b,Ball b1, Ball b2){
		/*
		 * necesitamos conectar las dos bolitan con una imagen que se estirara
		 * 
		 */
		this.B1 = b1;
		this.B2 = b2;
		this.bundle = b; 
		this.conex = new Image(bundle.conexion); // creamos la imagen.
		this.loadPositions();
		this.bundle.balls.addActor(this);
		this.bundle.balls.addActor(conex);
		this.conex.toBack();
	}
	public void loadPositions(){
		float w2 = B1.ball.getScaleX()*B1.ball.getWidth()/2;
		float h2 = B1.ball.getHeight()/2;
		deltax = (float) Math.pow(B1.ball.getX() - B2.ball.getX(),2 )  ;
		deltay = (float) Math.pow(B1.ball.getY() - B2.ball.getY(),2 ) ;
		dis = (float) Math.sqrt(deltax + deltay); 
		scale = dis/conex.getWidth();
		angle = (float)Math.atan( Math.sqrt(deltay/deltax));
		if(B1.ball.getX() <= B2.ball.getX()  ){
			x = B1.ball.getX();
			y = B1.ball.getY();
			//x -= A.vertxImg.getWidth()/2;
			if(y > B2.ball.getY() ) angle *= -1;
		}else 		
		{  // if(B.getPositionVertex().x < A.getPositionVertex().x  ) 

			x = B2.ball.getX();
			y = B2.ball.getY();
			//x -= B.vertxImg.getWidth()/2;
			if(y > B1.ball.getY()) angle *= -1;
		}
		
		this.conex.setWidth(dis);
		this.conex.setPosition(x+w2, y+h2);
		this.conex.setRotation(angle*57.2f);
		
	}
	@Override
	public void act(float delta){
		super.act(delta);
		this.loadPositions(); // actualizar posiciones.
		
	}
}
