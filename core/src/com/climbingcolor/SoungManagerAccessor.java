package com.climbingcolor;

import aurelienribon.tweenengine.TweenAccessor;

public class SoungManagerAccessor implements TweenAccessor<SoundManager> {
public static final int VOLUME = 1;
	@Override
	public int getValues(SoundManager arg0, int arg1, float[] arg2) {
		// TODO Auto-generated method stub
		switch(arg1){
		case VOLUME:
			arg2[0] = arg0.volume;
			return 1;
		default : assert false; return -1;	
		}
		
	}

	@Override
	public void setValues(SoundManager arg0, int arg1, float[] arg2) {
		// TODO Auto-generated method stub
		switch(arg1){
		case VOLUME:
			arg0.volume = arg2[0];
			break;
		default: assert false; break;	
		}
	}

}
