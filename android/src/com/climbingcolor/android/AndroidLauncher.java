package com.climbingcolor.android;



import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.badlogic.gdx.backends.android.AndroidApplication;
import com.badlogic.gdx.backends.android.AndroidApplicationConfiguration;
import com.badlogic.gdx.backends.android.AndroidFragmentApplication;
import com.climbingcolor.AndroidInterface;
import com.climbingcolor.MainClimb;
import com.facebook.FacebookRequestError;
import com.facebook.HttpMethod;
import com.facebook.Request;
import com.facebook.Response;
import com.facebook.Session;
import com.facebook.SessionState;
import com.facebook.UiLifecycleHelper;
import com.facebook.model.GraphUser;
import com.facebook.widget.FacebookDialog;
import com.facebook.widget.LoginButton;
import com.revmob.RevMob;
import com.revmob.RevMobAdsListener;
import com.revmob.RevMobTestingMode;
import com.revmob.ads.fullscreen.RevMobFullscreen;
import com.revmob.ads.link.RevMobLink;



public class AndroidLauncher extends AndroidApplication implements AndroidInterface {
	MainClimb main;
	Handler fHandler;
	RevMob revmob;
	RevMobFullscreen fullscreen;
	RevMobLink link;
	LoginButton login; // for login
	PublishAction action;
	private UiLifecycleHelper uihelper;
	boolean isOpened = false;
	private static final String PERMISSION = "publish_actions";
	//53de88b8cda3eeab0617fe16 
	// fb 423105897827771
	private enum PublishAction{
		NONE,
		publishScore
	}
	 private Session.StatusCallback callback = new Session.StatusCallback() {
	        @Override
	        public void call(Session session, SessionState state, Exception exception) {
	           // onSessionStateChange(session, state, exception);
	        	if(state == SessionState.OPENED) {
	        		isOpened = true;
	        		runOnUiThread( new Runnable(){

						@Override
						public void run() {
							// TODO Auto-generated method stub
							Toast.makeText(getContext(), "Session Opened!", Toast.LENGTH_SHORT).show();
						}
	        			
	        		});
	        		
	        		requestPermissions();
	        		// Session.NewPermissionsRequest
	        	}
	        	
	        	
	        }
	    };
	 private FacebookDialog.Callback dialogCallback = new FacebookDialog.Callback() {
	        @Override
	        public void onError(FacebookDialog.PendingCall pendingCall, Exception error, Bundle data) {
	            Log.d("HelloFacebook", String.format("Error: %s", error.toString()));
	        }

	        @Override
	        public void onComplete(FacebookDialog.PendingCall pendingCall, Bundle data) {
	            Log.d("HelloFacebook", "Success!");
	        }
	    };   
	@Override
	protected void onCreate (Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		AndroidApplicationConfiguration config = new AndroidApplicationConfiguration();
		revmob = RevMob.start(this);
		//revmob.setTestingMode(RevMobTestingMode.WITH_ADS); // test
		// -> LOAD AD fullscreen = revmob.createFullscreen(this, null);
		link = revmob.createAdLink(this, getListener());
		fullscreen = revmob.createFullscreen(this, null);
		/*
		 * view shit
		 */
	
		/*
		 * facebook shit
		 */
		uihelper = new UiLifecycleHelper(this,callback);
		login = new LoginButton(this.getContext());//(LoginButton) this.findViewById(R.id.login_button);
	      login.setUserInfoChangedCallback(new LoginButton.UserInfoChangedCallback() {
	            @Override
	            public void onUserInfoFetched(GraphUser user) {
	               // HelloFacebookSampleActivity.this.user = user;
	                //updateUI();
	                // It's possible that we were waiting for this.user to be populated in order to post a
	                // status update.
	              //   handlePendingAction();
	            }
	        });
		main = new MainClimb(this);
		initialize(main, config);
		action = PublishAction.NONE;
		
	}
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);
		this.uihelper.onActivityResult(requestCode, resultCode, data, dialogCallback);
		}
	@Override
	public void postScore(int score) {
		// TODO Auto-generated method stub
		//this.login("aaaa");
		if(this.hasPublishPermission()){
			// existe la session y la podemos escribir:
			this.requestPermissions();
			String message = " I taped " + Integer.toString(score) + " red balls ! "; 
			Bundle postPara = new Bundle();
			postPara.putString("name", "Climbing Color");
			postPara.putString("caption", message);
			postPara.putString("description", "Tap the red ball before the time end!");
			postPara.putString("link", "https://play.google.com/store/apps/details?id=com.climbingcolor.android");
			Request request = Request.newStatusUpdateRequest(Session.getActiveSession(), message, new Request.Callback() {
				
				@Override
				public void onCompleted(Response response) {
					// TODO Auto-generated method stub
					Log.println(LOG_INFO, "Climb", "AHHH");
					runOnUiThread(new Runnable(){

						@Override
						public void run() {
							// TODO Auto-generated method stub
							Toast.makeText(getContext(), "Score published!", Toast.LENGTH_SHORT).show();;
						}
						
					});
					
				}
			}); 
			//request.executeAndWait();
			Request request2 = new Request(Session.getActiveSession() , "me/feed" , postPara , HttpMethod.POST , callbackR);
			request2.executeAndWait();
		}
	}
	@Override
	public void loadAd() {
		// TODO Auto-generated method stub
		fullscreen = revmob.createFullscreen(this, null);
	}
	@Override
	public void showAd() {
		// TODO Auto-generated method stub
		if(fullscreen != null) {
			fullscreen.show();
		}else {
			loadAd();
		}
	}
	@Override
	public void showMoreGames() {
		// TODO Auto-generated method stub
		if(link != null){
			link.open();
		} 
	}
	private boolean hasPublishPermission(){
		Session session = Session.getActiveSession();
		return (session != null );//&& session.getPermissions().contains("publish_actions") );
	}
	public boolean isSession(){
		return true;
	}
	public RevMobAdsListener getListener(){
		RevMobAdsListener listener = new RevMobAdsListener(){

			@Override
			public void onRevMobAdClicked() {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void onRevMobAdDismiss() {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void onRevMobAdDisplayed() {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void onRevMobAdNotReceived(String arg0) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void onRevMobAdReceived() {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void onRevMobEulaIsShown() {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void onRevMobEulaWasAcceptedAndDismissed() {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void onRevMobEulaWasRejected() {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void onRevMobSessionIsStarted() {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void onRevMobSessionNotStarted(String arg0) {
				// TODO Auto-generated method stub
				
			}
			
		};
		return listener;
	}
	@Override
	public void startLoginLibgdx(int score) {
		// TODO Auto-generated method stub
		if(!this.hasPublishPermission()) {
		login.performClick();
		}else {
			this.postScore(score);
		}
	}
	private void requestPermissions(){
		Session session = Session.getActiveSession();
		if(session.getPermissions().contains("publish_actions")) return ;
		 session.requestNewPublishPermissions( new Session.NewPermissionsRequest( this , PERMISSION));
	}
	
	private Request.Callback callbackR = new Request.Callback() {
		
		@Override
		public void onCompleted(Response response) {
			// TODO Auto-generated method stub
			
			if(response == null){
				runOnUiThread(new Runnable(){

					@Override
					public void run() {
						// TODO Auto-generated method stub
						
						Toast.makeText(getContext(), "Feature still not available. try later", Toast.LENGTH_SHORT).show();;
					}
					
				});
				return ;
			}
			runOnUiThread(new Runnable(){

				@Override
				public void run() {
					// TODO Auto-generated method stub
					
					Toast.makeText(getContext(), "Score shared!", Toast.LENGTH_SHORT).show();;
				}
				
			});
		}
	};
}
