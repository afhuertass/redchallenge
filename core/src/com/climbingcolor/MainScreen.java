package com.climbingcolor;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.ui.Label;

import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.viewport.FillViewport;

public class MainScreen implements Screen{
	MainClimb game;
	boolean isLoaded = false;
	Stage stage;
	Group elements = new Group();
	Image fondo;
	ImageButton play;
	Label timeLabel;
	Image timer;
	Image redl;
	Status st;
	Bundle bundle;
	Image inicio;
	Image loading;
	MainScreen(MainClimb game){
		this.game = game;
		this.game.loader.startLoad();
		st = Status.start;
		bundle = new Bundle(this.game);
	}
	private enum Status{
		play,
		over,
		start,
		done
	}
	public void loadInicio(){
		this.inicio = new Image( new TextureRegionDrawable( game.loader.manager.get("data/atlas/inicio.pack" , TextureAtlas.class).findRegion("inicio")));
		inicio.setPosition(100, 600);
		stage.addActor(inicio);
		inicio.addAction(Actions.sequence(  Actions.fadeOut(0.00001f) , Actions.fadeIn(2f) , Actions.fadeOut(2f) , Actions.run(new Runnable(){

			@Override
			public void run() {
				// TODO Auto-generated method stub
				st = Status.done;
			}
			
		})));
	}
	
	@Override
	public void render(float delta) {
		// TODO Auto-generated method stub
		Gdx.gl.glClearColor( 0f, 0f, 0f, 1f );
        Gdx.gl.glClear( GL20.GL_COLOR_BUFFER_BIT );
		game.loader.manager.update();
		if(game.loader.isLoad() && !isLoaded && st == Status.done){
			isLoaded = true;
			this.loadElements();
		}
		switch(st){
		case play:
			bundle.update(delta);
			if(!bundle.soundManager.isLoadingSound()) {
				this.loading.rotateBy(1000*delta);
				this.loading.setVisible(true);
			}else { // FACEBOOK TEST
				loading.setVisible(false);
			}
			break;
		case over:
			
			break;
		default: break;	
			
		}
		stage.act(delta);
		stage.draw();
	}
	public void organize(){
		
	}
	public void loadElements(){
		this.fondo = new Image(game.loader.manager.get("data/atlas/main.pack" , TextureAtlas.class).findRegion("background4") );
		this.loading = new Image(game.loader.manager.get("data/atlas/main.pack" , TextureAtlas.class).findRegion("cargando") );
		this.play = new ImageButton( new TextureRegionDrawable(game.loader.manager.get("data/atlas/main.pack", TextureAtlas.class).findRegion("playOn") ) , new TextureRegionDrawable(game.loader.manager.get("data/atlas/main.pack", TextureAtlas.class).findRegion("play") ) );
		this.timer = new Image(game.loader.manager.get("data/atlas/main.pack" , TextureAtlas.class).findRegion("time")); 
		//test
		ParticleActor ex = new ParticleActor("data/effects/effect1" , game.loader.manager.get("data/atlas/main.pack" , TextureAtlas.class ));
		this.play.setPosition(120, 500);
		this.timer.setPosition(20, 1200);
		this.loading.setPosition(20, 100);
		this.loading.setOrigin(loading.getX() + loading.getWidth()/2, loading.getY() + loading.getHeight()/2);
		this.loading.setVisible(false);
		elements.addActor(fondo); elements.addActor(play); elements.addActor(loading);
		//elements.addActor(timer); // REMOVER TIMER POR COMPLETO
		stage.addActor(elements);
		stage.addActor(bundle.balls);
		bundle.loadTextures();
		
		//elements.addActor(bundle.timeBar);
		//elements.addActor(bundle.timeBarRed);
		bundle.timeBar.toFront();
		stage.addActor(bundle.gameOverE);
		stage.addActor(bundle.timeBars);
		stage.addActor(ex);
		this.redl = new Image(bundle.red);
		redl.setPosition(10, 1110);
		redl.setScale(0.9f);
		elements.addAction(Actions.sequence( Actions.fadeOut(0.00001f) , Actions.fadeIn(1f)));
		//elements.addActor(redl); REMOVER REDL POR COMPLETO
		this.addListeners();
	}
	public void addListeners(){
		play.addListener(new InputListener(){
		@Override
		public boolean touchDown(InputEvent event, float x, float y, int pointer, int button){
			st = Status.play;
			play.addAction(Actions.fadeOut(0.5f));
			//bundle.scoreoidManager.getBestScore();
			return true;
		}
		});
	}
	@Override
	public void resize(int width, int height) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void show() {
		// TODO Auto-generated method stub
		stage = new Stage(new FillViewport( 720,1280));
		Gdx.input.setInputProcessor(stage);
		loadInicio();
	}

	@Override
	public void hide() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void dispose() {
		// TODO Auto-generated method stub
		
	}
	
	

}
