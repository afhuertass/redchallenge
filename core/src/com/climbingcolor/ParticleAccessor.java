package com.climbingcolor;

import aurelienribon.tweenengine.TweenAccessor;

public class ParticleAccessor implements TweenAccessor<ParticleActor>{
public static final int POSITION_X = 1;
	@Override	
	public int getValues(ParticleActor arg0, int arg1, float[] arg2) {
		// TODO Auto-generated method stub
		switch(arg1){
		case POSITION_X:
			arg2[0] = arg0.getX();
			return 1;
		default: assert false; return -1;	
		}
		
	}

	@Override
	public void setValues(ParticleActor arg0, int arg1, float[] arg2) {
		// TODO Auto-generated method stub
		switch(arg1){
		case POSITION_X:
			arg0.setX( arg2[0] );
			break;
		default: assert false; break ;	
		}
	}

}
