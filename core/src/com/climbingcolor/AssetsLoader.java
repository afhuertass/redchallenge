package com.climbingcolor;

import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;

public class AssetsLoader {
/*
 * como antes cargara el atlas, la musica y las fuentes
 */
	public AssetManager manager = new AssetManager();
	AssetsLoader(){
		
	}
	public void startLoad(){
		this.loadInicio();
		
		this.loadMain();
		this.loadFonts();
		this.loadMusic();
	}
	private void loadInicio(){
		manager.load("data/atlas/inicio.pack" , TextureAtlas.class);
		manager.finishLoading();
	}
	public void loadMusic(){
		manager.load("data/sounds/builder.ogg" , Music.class);
		manager.load("data/sounds/ting.mp3" , Sound.class);
		manager.load("data/sounds/popB.mp3" , Sound.class);
		manager.load("data/sounds/wrong1.ogg" , Sound.class);
	}
	public void loadMain(){
		manager.load("data/atlas/main.pack" , TextureAtlas.class);
	}
	public void loadFonts(){
		manager.load("data/fonts/font.fnt" , BitmapFont.class);
	}
	public void loadSounds(){
		manager.load("data/sounds/corr.ogg" , Sound.class);
	}
	public boolean isLoad(){
		if(manager.isLoaded("data/atlas/main.pack") && manager.isLoaded("data/fonts/font.fnt")){
			return true;
		}
		return false;
	}
}
