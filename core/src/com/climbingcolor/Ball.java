package com.climbingcolor;



import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;

import com.badlogic.gdx.scenes.scene2d.ui.Image;


public class Ball extends Actor{
/*
 * la bolita, tiene la imagen y el color. y en constructor recible en bundle.
 * la imagen al se clickeada dispara un evento en el bundle, que verifica si es roja o no.
 * si es roja debe hacer lo de bajar las bolitas y agregar las nuevas si no, game over.
 * 
 * color; 0 = roja; 1 = azul; 2 = amarilla; 3=  naranja.
 *  carga la textura de 
 */
	
	Bundle bundle;
	Image ball;
	int color;
	float deltaY = 0;
	boolean isTouched = false;
	public int colum = 0;
	float omega = 0;
	Ball(Bundle b, float x, float y){
		this.bundle = b; 
		color = bundle.random.nextInt(3);
		ball = new Image(bundle.getTexture(color));
		ball.setScale(1.8f);
		ball.setPosition(x, y);
		ball.setOrigin(ball.getWidth()/2, ball.getHeight()/2);
		omega = 50*bundle.random.nextFloat();
		this.addListener();
	}
	public void addListener(){
		ball.addListener(new InputListener(){
			public boolean touchDown(InputEvent event, float x, float y, int pointer, int button){
				// cuando la toca. 
				if(color != 0){
					// game over. si no es rojo
					bundle.time -= 10;
					ball.setScale(1f);
					//bundle.showParticleEnd(ball.getX(), ball.getY());
					bundle.soundManager.playWrong();
					return true;
				}
				if(isTouched) return true;
				if(bundle.isOver) return true;
				if(bundle.selected != null){
					// check slalom
					bundle.checkSlalom(getBall());
				}else {
					
					if(getBall().colum == 0 || getBall().colum == 2) bundle.counterSlalom+=1;
				}	
				
			
				if(bundle.selected != null) {
					if(getBall().ball.getY() < bundle.selected.ball.getY()) {
						// si le da a una atras, pierde.
						bundle.time = 0;
						return true;
					}
					if(getBall().ball.getY() == bundle.selected.ball.getY()){
						bundle.tweenManager.killAll();
					}
					
					bundle.score += 1;
					bundle.time += 0.25;
					bundle.soundManager.playPop();
					bundle.showParticle(ball.getX()+ball.getWidth()/2, ball.getY()+ball.getHeight()/2);
					isTouched = true;
					new Conexion(bundle , bundle.selected , getBall());
				}
				bundle.checkCombo();
				setSelected();
				bundle.selected = getBall(); // el seleccionado es esta.
				bundle.calculateAllDeltas(); // calculamos los deltas de todos.
				// aqu{i hay que proceder al tween. bajar el 
				bundle.startTween(); 
				return true;
			}
		});
	}
	@Override
	public void act(float delta){
		super.act(delta);
		if(bundle.selected == null) return; // si no hay nada seleccionado
		if(this.equals(bundle.selected)) return;
		float newDelta = ball.getY() - bundle.selected.ball.getY(); // nuevo delta.
		float amount = newDelta-deltaY;
		ball.setY(ball.getY() - Math.abs(amount));
		ball.rotateBy(omega*delta);
	}
	public void getDelta(){
		if(bundle.selected == null ) return;
		deltaY = ball.getY() - bundle.selected.ball.getY();
	}
	public Ball getBall(){
		return this;
	}
	public void setNormal(){
		this.ball.setDrawable(bundle.red);
	}
	public void setSelected(){
		this.ball.setDrawable(bundle.redOn);
	}
	public void turnRed(){
		
	}
}
