package com.climbingcolor.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.climbingcolor.AndroidInterface;
import com.climbingcolor.MainClimb;

public class DesktopLauncher {
	public static void main (String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		config.width = 480;	
		config.height = 860;
		AndroidInterface i = null;
		new LwjglApplication(new MainClimb(i), config);
	}
}
