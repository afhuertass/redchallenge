package com.climbingcolor;

import com.badlogic.gdx.Preferences;

public class ScoreManager {

	Preferences main;
	ScoreManager(Preferences main){
		// entonces. 
		this.main = main;
		if(main.getInteger("bsco") != 0) return;
		main.putInteger("bsco", 0); // test
		main.putInteger("sck", 0);
		main.flush();
	}
	
	public int getHighScore(){
		return this.main.getInteger("bsco");
	}
	public boolean saveScore(int score){
		if(score <= getHighScore()){
			return false ;
		}else {
			// save
			int key = main.getInteger("sck");
			if(key == 0){
			this.main.putInteger("bsco", score);
			this.main.putInteger("sck", 9966*score);
			this.main.flush();
			}else {
				if(key == this.getHighScore()*9966){
					// todo bien
					this.main.putInteger("bsco", score);
					this.main.putInteger("sck", 9966*score);
					this.main.flush();
				}
			}
			return true;
		}
	}
	
	public void savePlayerId(int id){
		this.main.putInteger("user", id);
		this.main.flush();
	}
	public int getUserId(){
		
		return main.getInteger("user");
	}
}
